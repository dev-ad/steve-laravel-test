<?php
# routes\web.php #####################################
Route::get('/test', [PostController::class, 'test']);

# App\Http\Controllers\PostController.php #####################################
namespace App\Http\Controllers;

use App\Services\PostService;

class PostController extends Controller
{
    private $service;

    private function __construct ( PostService $service ) {
        $this->service = $service;
    }

    public function test()
    {
    	// Can we get this far?
        // $all_posts = $this->service->getAll();
    }
}

# App\Repositories\PostRepository.php #####################################
namespace App\Repositories;

use App\Models\Post;
use Illuminate\Database\Eloquent\Collection;

class PostRepository {

	private $model;

	public function __construct ( Post $model ) {
		$this->model = $model;
	}

	public function getAll() : Collection {
		return $this->model->all();
	}
}

# App/Services/PostService.php #####################################
namespace App\Services;

use App\Repositories\PostRepository;
use Illuminate\Database\Eloquent\Collection;

class PostService {

	# Service

	private $repository;

	public function __construct ( PostRepository $repository ) {
		$this->repository = $repository;
	}

	public function getAll() : Collection {
		return $this->repository->getAll();
	}
}