<x-layout>
    {{-- @dd($post->title) --}}
    <div class="container py-3 justify-content-center">
        <h1 class="text-center">Edit a Post</h1>

        <div class="d-flex flex: 0 0 200px; justify-content-center p-2">

            {{-- <div></div> --}}

            <div class="p-2 border border-2 border-dark" style="background-color:#E8F3FC; border-radius:12px;  flex: 0 0 500px;">
                <div class="px-5">
                    
                    <form action="{{-- route('posts.update' ) --}}" method="POST">
                        @csrf
                        @method('POST')
                        <div class="form-group">
                            <label for="title">Post Title</label>
                            <input type="text" class="form-control" name="title" id="title" value="{{ $post->title }}">
                            @error('title')
                                <p class="text-danger text-xs"><strong>{{ $message }}</strong></p>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="body">Description</label>
                            <textarea style="resize: none;" class="form-control rounded-0" name="body" id="body" rows="8">{{ $post->body }}</textarea>
                            @error('body')
                                <p class="text-danger text-xs"><strong>{{ $message }}</strong></p>
                            @enderror
                        </div> 

                        <div class="d-flex">
                            <div><button type="submit" class="btn btn-success mt-3">Update</button></div>
                            <div class="p-3"><a href="{{ url()->previous() }}" class="btn btn-danger">Go Back</a></div>
                        </div>

                        <input type="hidden" id="user_id" name="user_id" value="{{ $post->user_id }}">

                    </form>

                </div>
            </div>

            {{-- <div></div> --}}
        </div>
        
        @if (session()->has('message'))
            <div class="d-flex justify-content-center mt-5" id="hideDiv">
                <div class="p-2">
                    <p>{{ session('message') }}</p>
                </div>
            </div>
        @endif

    </div>

    {{-- </div> --}}

</x-layout>