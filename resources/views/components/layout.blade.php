<!doctype html>
<html>
<head>
<title>Posts</title>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"><link rel="preconnect" href="https://fonts.gstatic.com">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet">
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script type="text/javascript" src="https://getbootstrap.com/docs/4.0/assets/js/vendor/popper.min.js"></script>
<script type="text/javascript" src="https://getbootstrap.com/docs/4.0/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://getbootstrap.com/docs/4.0/assets/js/vendor/holder.min.js"></script>

<script type="text/javascript">

    function display_c(){
        var refresh=1000; // Refresh rate in milli seconds
        mytime=setTimeout('display_ct()',refresh)
    }

    function display_ct() {
        var x = new Date()
        year = x.getFullYear();
        month = x.toLocaleString('default', { month: 'short' });
        month = month.toUpperCase();
        day = addZeroBefore(x.getUTCDate());
        x1 = (day + '-' + month + '-' + year);
        hours = addZeroBefore(x.getHours());
        minutes = addZeroBefore(x.getMinutes());
        seconds = addZeroBefore(x.getSeconds());
        x1 = x1 + " " +  hours + ":" +  minutes + ":" + seconds;
        document.getElementById('ct').innerHTML = x1;
        display_c();
    }

    function addZeroBefore(n) {
        return (n < 10 ? '0' : '') + n;
    }
    
</script>

<!-- Container Row Column -->
</head>
<body style="font-family: Open Sans, sans-serif" onload=display_ct();>

    <div>
        <div class="collapse" style="background-color: #000000" id="navbarHeader">

            <div class="d-flex justify-content-center text-light">
                
                <div class="p-2">
                    <ul">
                        <li><a href="/" class="text-white">Home Page</a></li>
                    </ul>
                </div>

                <div class="p-2">
                    <ul>
                        <li><a href="/posts" class="text-white">List Posts</a></li>
                    </ul>
                </div>

                <div class="p-2">
                    <!-- <ul class="list-unstyled"> -->
                    <ul>
                        <li><a href="/posts/create" class="text-white">Add a Post</a></li>
                    </ul>
                </div>

            </div>

        </div>

        <div class="navbar navbar-dark box-shadow" style="background-color: #2182D1">
            <div class="container d-flex justify-content-between">
                <a href="#" class="navbar-brand d-flex align-items-center">
                    <strong>Blog - Service Repository</strong>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </div>
    </div>

        {{ $slot }}

    <div class="d-flex justify-content-center text-light p-3 " style="background-color: #2182D1">
    {{-- <div class="d-flex justify-content-center text-light p-3 bg-cyan"> --}}
        <div>
            {{-- <p>FOOTER</p> --}}
            <strong><span id='ct' ></span></strong>
        </div>
    </div>

</body>

</html>