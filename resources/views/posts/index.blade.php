<x-layout>
    {{-- @dd($order) --}}
    <div>
        <div class="container pt-3 pb-4">
            <h1 class="text-center">Posts List</h1>

            <div class="p-2 border border-2 border-dark" style="background-color:#E8F3FC; border-radius:15px;">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th class="px-2">
                                <a href="{{ request()->fullUrlWithQuery(['by' => 'id', 'order' => $order === 'desc' ? 'asc' : 'desc']) }}">
                                @if ( $by === 'id' ) <i class="fa fa-arrow-{{ $order === 'asc' ? 'up' : 'down' }}"></i> @endif
                                 ID
                                </a>
                            </th>

                            <th class="px-2">
                                <a href="{{ request()->fullUrlWithQuery(['by' => 'name', 'order' => $order === 'desc' ? 'asc' : 'desc']) }}">
                                @if ( $by === 'name' ) <i class="fa fa-arrow-{{ $order === 'asc' ? 'up' : 'down' }}"></i> @endif
                                 Author
                            </th>

                            <th class="px-2">Title</th>
                            <th class="px-2">Date Posted</th>
                            <th class="px-2"></th>
                            <th class="px-2"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($all_posts as $post)
                            <tr>
                                <td class="px-2">{{ $post->id }}</td>
                                <td class="px-2">{{ $post->name }}</td>
                                <td class="px-2">{{ Str::limit($post->title, 50) }}</td>
                                <td class="px-2">{{ $post->created_at->diffForHumans() }}</td>
                                <td class="px-5"><a href="{{ route('posts.edit', $post->id) }}">Edit</a></td>
                                <td class="px-5"><a href="{{ route('posts.show', $post->id) }}">Delete</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{-- @if($posts instanceof \Illuminate\Pagination\LengthAwarePaginator ) --}}
                @if ($all_posts->hasPages())
                    {{ $all_posts->withQueryString()->links() }}
                @endif

            </div>

        </div>
    </div>

</x-layout>