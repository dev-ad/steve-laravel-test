@if (session()->has('message'))
    <!--<div class="d-flex justify-content-center align-items-center border border-dark mt-3 mb-2" style="background-color:#2182D1; color:#FFFFFF; border-radius:12px;" id="hideDiv">-->
    <div class="d-flex justify-content-center mt-3 mb-2" id="hideDiv">
        <div class=""><p>{{ session('message') }}</p></div>
    </div>
@endif