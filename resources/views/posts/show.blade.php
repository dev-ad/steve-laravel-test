<x-layout>

    <div class="container py-3 justify-content-center">
        <h1 class="text-center">Delete a Post</h1>

        <div class="d-flex flex: 0 0 200px; justify-content-center p-2">

            {{-- <div></div> --}}

            <div class="p-2 border border-2 border-dark" style="background-color:#E8F3FC; border-radius:12px;  flex: 0 0 500px;">
                <div class="px-5">

                    <form action="{{ route('posts.destroy', $post->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        @include('posts.form')

                        <div class="d-flex">
                            <div><button type="submit" class="btn btn-primary pb-2">Delete</button></div>
                            <div class="pb-3 pl-3"><a href="{{ $url_prev }}" class="btn btn-danger pb-2">< Cancel / Back</a></div>
                        </div>

                    </form>

                </div>

            </div>

            {{-- <div></div> --}}

        </div>

    </div>

</x-layout>