<input type="hidden" id="user_id" name="user_id" value="2">

<div class="form-group pt-2">
    <label for="title">Post Title</label>
    <input
        {{ $mode === "Delete" ? "readonly" : "" }}
        class="form-control"
        type="text"
        name="title"
        value="{{ old( 'title', $post->title ?? '' ) }}">
    @error('title')
        <p class="text-danger text-xs"><strong>{{ $message }}</strong></p>
    @enderror
</div>

<div class="form-group">
    <label for="body">Description</label>
    <textarea {{ $mode === "Delete" ? "readonly" : "" }} class="form-control rounded-0" name="body" style="resize: none;" rows="8">{{ old( 'body', $post->body ?? '') }}</textarea>
    @error('body')
        <p class="text-danger text-xs"><strong>{{ $message }}</strong></p>
    @enderror
</div> 