<x-layout>

    <div class="container py-3 justify-content-center">
        <h1 class="text-center">Create a Post</h1>

        <div class="d-flex flex: 0 0 200px; justify-content-center p-2">

            {{-- <div></div> --}}

            <div class="p-2 border border-2 border-dark" style="background-color:#E8F3FC; border-radius:12px;  flex: 0 0 500px;">
                <div class="px-5">
                    
                    <form action="{{ route('posts.store' ) }}" method="POST">
                        @csrf
                        @method('POST')
                        @include('posts.form')

                        <div class="d-flex pb-2">
                            <div><button type="submit" class="btn btn-primary pb-2">Add</button></div>
                        </div>
                        
                        @include('posts.form_message')

                    </form>

                </div>

            </div>

            {{-- <div></div> --}}

        </div>

    </div>

</x-layout>