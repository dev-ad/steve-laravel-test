<x-layout>
{{-- 20220118 10:43 --}}
    <main>

        <div>
            <div class="container py-5">
                <h1 class="text-center">Welcome to Blog - Service Repository</h1>
            </div>
            <div class="container pb-5 text-center">
                <img class="border border-dark" style="border-radius:10px;" src="{{asset("storage/blog.jpg")}}" title="symbolic link configured for image path">
            </div>
        </div>
 
    </main>

</x-layout>