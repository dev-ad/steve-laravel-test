<?php
namespace App\Repositories;

use App\Models\Post;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\Builder;

class PostRepository {

	# Repository will only talk to the database and service

	private $model;

	public function __construct ( Post $model ) {
		$this->model = $model;
	}

	public function getAll() : Collection {
		return $this->model->all();
	}

	public function getById( int $id ) : Post {
		// dd('hello');
		return $this->model->find( $id );
	}

	public function getAllPaginated( string $by, string $order, int $per_page, string $filter = null ) : LengthAwarePaginator {
		return $this->model->where( function ( $query ) use ( $filter ) {
			$this->applyFilter( $query->getQuery(), $filter );
		})
		->leftJoin( 'users', 'users.id', 'user_id' )
		// ->addSelect( 'users.name as name', 'posts.created_at', 'posts.id', 'posts.title' )
		->addSelect( 'users.name as name', 'posts.*' )
		->orderBy( $by, $order )
		->paginate( $per_page );
	}

	public function create( array $attributes ) : Post {
		return $this->model->create( $attributes );
	}

	public function update( int $id, array $attributes ) : bool {
		return $this->model->where( 'id', $id )->update( $attributes );
	}

	public function delete( int $id ) : bool {
		return $this->model->find( $id )->delete();
	}

	public function sync( Post $model, string $relation, array $relation_ids ) : array {
		return $model->$relation()->sync( $relation_ids );
	}

	public function applyFilter( Builder $query, ?string $filter ) : Builder {
		if ( !is_null( $filter )) {
			$query->orWhere( 'title', 'like', '%'.$filter.'%');
			$query->orWhere( 'text', 'like', '%'.$filter.'%');
		}
		return $query;
	}
}