<?php
namespace App\Services;

use App\Models\Post;
use App\Repositories\PostRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

class PostService {

	# Service

	private $repository;

	public function __construct ( PostRepository $repository ) {
		$this->repository = $repository;
	}

	public function getAll() : Collection {
		return $this->repository->all();
	}

	public function getById( int $id ) : Post {
		return $this->repository->getById( $id );
	}

	public function getAllPaginated( string $by, string $order, int $per_page, string $filter = null ) : LengthAwarePaginator {
		return $this->repository->getAllPaginated( $by, $order, $per_page, $filter );
	}

	public function create( array $attributes ) : Post {
		return $this->repository->create( $attributes );
	}

	public function update( int $id, array $attributes ) : bool {
		return $this->repository->update( $id, $attributes );
	}

	public function delete( int $id ) : bool {
		return $this->repository->delete( $id );
	}

	public function sync( Post $repository, string $relation, array $relation_ids ) : array {
		return $this->repository->sync( $id );
	}
}