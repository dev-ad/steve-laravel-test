<?php
namespace App\Http\Controllers;

use App\Models\Post;
use App\Services\PostService;
use Illuminate\Http\Request;
use Session;
use URL;

class PostController extends Controller
{
    private $postService;

    const ATTRIBUTES = [ 'title', 'body', 'user_id' ];

    const PER_PAGE = 10;
    const ORDER = 'asc';
    const BY = 'id';
    const NL = null;

    public function __construct ( PostService $postService ) {
        $this->postService = $postService;
    }

    public function index( Request $request )
    {
        if (Session::has('url_prev')):
            Session::forget('url_prev');
        endif;

        $by       = $request->input('by') ?? self::BY;
        $order    = $request->input('order') ?? self::ORDER;
        $per_page = $request->input('per_page') ?? self::PER_PAGE;
        $filter   = $request->input('filter') ?? null;

        $all_posts = $this->postService->getAllPaginated( $by, $order, $per_page, $filter );
        return view('posts.index', compact( 'all_posts', 'by', 'order', 'filter' ) );
    }

    public function destroy( int $id )
    {
        if (Session::has('url_prev')):
            $url_prev = Session::get('url_prev');
        else:
            $url_prev = URL::previous();
            Session::put('url_prev', $url_prev);
        endif;

        try {

            $post = $this->postService->getById( $id );
            $delete = $this->postService->delete( $id );

        } catch ( \Exception $exception ) {

            return redirect()->back()
                             ->with([
                                'status' => 'danger',
                                'message' => 'Error: ' . $exception->getMessage()
                            ])->withInput();
                             
        }

        return view( 'posts.confirm', [
            'post' => $post,
            'url_prev' => $url_prev,
            'mode' => 'Delete',
            'status' => 'success',
            'message' => 'The post has been successfully updated'
        ]);

    }        

    public function edit( int $id )
    {
        if (Session::has('url_prev')):
            $url_prev = Session::get('url_prev');
        else:
            $url_prev = URL::previous();
            Session::put('url_prev', $url_prev);
        endif;

        $post = $this->postService->getById( $id );
        return view( 'posts.edit', compact( 'post' ), [
            'url_prev' => $url_prev,
            'mode' => 'Update'
        ]);
    }
         
    public function show( int $id )
    {
        if (Session::has('url_prev')):
            $url_prev = Session::get('url_prev');
        else:
            $url_prev = URL::previous();
            Session::put('url_prev', $url_prev);
        endif;

        $post = $this->postService->getById( $id );
        return view( 'posts.show', compact( 'post' ), [
            'url_prev' => $url_prev,
            'mode' => 'Delete'
        ]);
    }        


    public function update( Request $request, int $id )
    {
        $attributes = request()->validate([
            'title' => ['required'],
            'body' => ['required'],
            'user_id' => ['required']
        ]);

        try {

            $attributes = $request->only( self::ATTRIBUTES );
            $this->postService->update( $id, $attributes );

        } catch ( \Exception $exception ) {

            return redirect()->back()
                             ->with([
                                'status' => 'danger',
                                'message' => 'Error: ' . $exception->getMessage()
                            ])->withInput();
                             
        }

        return redirect()->back()
                         ->with([
                            'status' => 'success',
                            'message' => 'The post has been successfully updated'
                        ]);

    }        

    public function create()
    {

        $url_prev = "";
        return view( 'posts.create' , [
            'url_prev' => $url_prev,
            'mode' => 'Update'
        ]);

    }        

    public function store( Request $request )
    {

        $attributes = request()->validate([
            'title' => ['required'],
            'body' => ['required'],
            'user_id' => ['required']
        ]);

        try {

            $attributes = $request->only( self::ATTRIBUTES );
            $new_post = $this->postService->create( $attributes );

        } catch ( \Exception $exception ) {

            return redirect()->back()
                             ->with([
                                'status' => 'danger',
                                'message' => 'Error: ' . $exception->getMessage()
                            ])->withInput();

        }

        return redirect()->route( 'posts.edit', $new_post->id )
                             ->with([
                                'status' => 'success',
                                'message' => 'A new Post has been created'
                            ]);

    }    
}